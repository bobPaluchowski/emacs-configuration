(define-package "modus-themes" "20230922.531" "Elegant, highly legible and customizable themes"
  '((emacs "27.1"))
  :commit "daf689493e203f9b97211c4c13d472ebd7a9a0b7" :authors
  '(("Protesilaos Stavrou" . "info@protesilaos.com"))
  :maintainers
  '(("Modus-Themes Development" . "~protesilaos/modus-themes@lists.sr.ht"))
  :maintainer
  '("Modus-Themes Development" . "~protesilaos/modus-themes@lists.sr.ht")
  :keywords
  '("faces" "theme" "accessibility")
  :url "https://git.sr.ht/~protesilaos/modus-themes")
;; Local Variables:
;; no-byte-compile: t
;; End:

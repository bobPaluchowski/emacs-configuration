(define-package "doom-modeline" "20230803.1527" "A minimal and modern mode-line"
  '((emacs "25.1")
    (compat "28.1.1.1")
    (nerd-icons "0.0.1")
    (shrink-path "0.2.0"))
  :commit "bcf99afdf258068869422da421294bdf9052712f" :authors
  '(("Vincent Zhang" . "seagle0128@gmail.com"))
  :maintainers
  '(("Vincent Zhang" . "seagle0128@gmail.com"))
  :maintainer
  '("Vincent Zhang" . "seagle0128@gmail.com")
  :keywords
  '("faces" "mode-line")
  :url "https://github.com/seagle0128/doom-modeline")
;; Local Variables:
;; no-byte-compile: t
;; End:

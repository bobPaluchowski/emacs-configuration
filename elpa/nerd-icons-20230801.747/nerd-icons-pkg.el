(define-package "nerd-icons" "20230801.747" "Emacs Nerd Font Icons Library"
  '((emacs "24.3"))
  :commit "60ed056a72a4a93e75be32d4727be047eb2bce40" :authors
  '(("Hongyu Ding <rainstormstudio@yahoo.com>, Vincent Zhang" . "seagle0128@gmail.com"))
  :maintainers
  '(("Hongyu Ding <rainstormstudio@yahoo.com>, Vincent Zhang" . "seagle0128@gmail.com"))
  :maintainer
  '("Hongyu Ding <rainstormstudio@yahoo.com>, Vincent Zhang" . "seagle0128@gmail.com")
  :keywords
  '("lisp")
  :url "https://github.com/rainstormstudio/nerd-icons.el")
;; Local Variables:
;; no-byte-compile: t
;; End:

(define-package "modus-themes" "20230610.243" "Elegant, highly legible and customizable themes"
  '((emacs "27.1"))
  :commit "4ef638c53b1eb35e8861da6bae47dca4584d2941" :authors
  '(("Protesilaos Stavrou" . "info@protesilaos.com"))
  :maintainers
  '(("Modus-Themes Development" . "~protesilaos/modus-themes@lists.sr.ht"))
  :maintainer
  '("Modus-Themes Development" . "~protesilaos/modus-themes@lists.sr.ht")
  :keywords
  '("faces" "theme" "accessibility")
  :url "https://git.sr.ht/~protesilaos/modus-themes")
;; Local Variables:
;; no-byte-compile: t
;; End:

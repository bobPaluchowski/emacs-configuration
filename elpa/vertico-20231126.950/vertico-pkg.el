(define-package "vertico" "20231126.950" "VERTical Interactive COmpletion"
  '((emacs "27.1")
    (compat "29.1.4.4"))
  :commit "d9eaa3043907cb266e23f2cf3f70032230e0392b" :authors
  '(("Daniel Mendler" . "mail@daniel-mendler.de"))
  :maintainers
  '(("Daniel Mendler" . "mail@daniel-mendler.de"))
  :maintainer
  '("Daniel Mendler" . "mail@daniel-mendler.de")
  :keywords
  '("convenience" "files" "matching" "completion")
  :url "https://github.com/minad/vertico")
;; Local Variables:
;; no-byte-compile: t
;; End:

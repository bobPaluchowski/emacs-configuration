(define-package "vertico" "20230923.2021" "VERTical Interactive COmpletion"
  '((emacs "27.1")
    (compat "29.1.4.0"))
  :commit "8ba2be846274a8128012fbdc688fae3aa51793b0" :authors
  '(("Daniel Mendler" . "mail@daniel-mendler.de"))
  :maintainers
  '(("Daniel Mendler" . "mail@daniel-mendler.de"))
  :maintainer
  '("Daniel Mendler" . "mail@daniel-mendler.de")
  :keywords
  '("convenience" "files" "matching" "completion")
  :url "https://github.com/minad/vertico")
;; Local Variables:
;; no-byte-compile: t
;; End:

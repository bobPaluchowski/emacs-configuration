(define-package "no-littering" "20230620.1017" "Help keeping ~/.config/emacs clean"
  '((emacs "25.1")
    (compat "29.1.4.1"))
  :commit "cc56893faaf769226ceae8836a426da583fab992" :authors
  '(("Jonas Bernoulli" . "jonas@bernoul.li"))
  :maintainers
  '(("Jonas Bernoulli" . "jonas@bernoul.li"))
  :maintainer
  '("Jonas Bernoulli" . "jonas@bernoul.li")
  :keywords
  '("convenience")
  :url "https://github.com/emacscollective/no-littering")
;; Local Variables:
;; no-byte-compile: t
;; End:

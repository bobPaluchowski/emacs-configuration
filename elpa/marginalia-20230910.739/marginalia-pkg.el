(define-package "marginalia" "20230910.739" "Enrich existing commands with completion annotations"
  '((emacs "27.1")
    (compat "29.1.4.0"))
  :commit "f1734375a5d8fa18e9cecb47ae4b5ae86c72399f" :authors
  '(("Omar Antolín Camarena <omar@matem.unam.mx>, Daniel Mendler" . "mail@daniel-mendler.de"))
  :maintainers
  '(("Omar Antolín Camarena <omar@matem.unam.mx>, Daniel Mendler" . "mail@daniel-mendler.de"))
  :maintainer
  '("Omar Antolín Camarena <omar@matem.unam.mx>, Daniel Mendler" . "mail@daniel-mendler.de")
  :keywords
  '("docs" "help" "matching" "completion")
  :url "https://github.com/minad/marginalia")
;; Local Variables:
;; no-byte-compile: t
;; End:

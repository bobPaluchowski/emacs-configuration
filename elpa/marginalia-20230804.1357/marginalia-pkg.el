(define-package "marginalia" "20230804.1357" "Enrich existing commands with completion annotations"
  '((emacs "27.1")
    (compat "29.1.4.0"))
  :commit "7644e76191c58177d040a0af58ef949ad39e6467" :authors
  '(("Omar Antolín Camarena <omar@matem.unam.mx>, Daniel Mendler" . "mail@daniel-mendler.de"))
  :maintainers
  '(("Omar Antolín Camarena <omar@matem.unam.mx>, Daniel Mendler" . "mail@daniel-mendler.de"))
  :maintainer
  '("Omar Antolín Camarena <omar@matem.unam.mx>, Daniel Mendler" . "mail@daniel-mendler.de")
  :keywords
  '("docs" "help" "matching" "completion")
  :url "https://github.com/minad/marginalia")
;; Local Variables:
;; no-byte-compile: t
;; End:

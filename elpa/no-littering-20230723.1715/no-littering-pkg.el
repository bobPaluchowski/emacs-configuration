(define-package "no-littering" "20230723.1715" "Help keeping ~/.config/emacs clean"
  '((emacs "25.1")
    (compat "29.1.4.1"))
  :commit "b8ed106a981a4460a378eaf8f131df72916af94d" :authors
  '(("Jonas Bernoulli" . "jonas@bernoul.li"))
  :maintainers
  '(("Jonas Bernoulli" . "jonas@bernoul.li"))
  :maintainer
  '("Jonas Bernoulli" . "jonas@bernoul.li")
  :keywords
  '("convenience")
  :url "https://github.com/emacscollective/no-littering")
;; Local Variables:
;; no-byte-compile: t
;; End:

(define-package "nerd-icons" "20230811.548" "Emacs Nerd Font Icons Library"
  '((emacs "24.3"))
  :commit "8f0d9a3c7d89d5cbb75f23286e44a56679c1e28a" :authors
  '(("Hongyu Ding <rainstormstudio@yahoo.com>, Vincent Zhang" . "seagle0128@gmail.com"))
  :maintainers
  '(("Hongyu Ding <rainstormstudio@yahoo.com>, Vincent Zhang" . "seagle0128@gmail.com"))
  :maintainer
  '("Hongyu Ding <rainstormstudio@yahoo.com>, Vincent Zhang" . "seagle0128@gmail.com")
  :keywords
  '("lisp")
  :url "https://github.com/rainstormstudio/nerd-icons.el")
;; Local Variables:
;; no-byte-compile: t
;; End:

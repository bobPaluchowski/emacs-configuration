(define-package "no-littering" "20230611.1653" "Help keeping ~/.config/emacs clean"
  '((emacs "25.1")
    (compat "29.1.4.1"))
  :commit "44cbb50a8b40df90add21474c3558f1de122c51c" :authors
  '(("Jonas Bernoulli" . "jonas@bernoul.li"))
  :maintainers
  '(("Jonas Bernoulli" . "jonas@bernoul.li"))
  :maintainer
  '("Jonas Bernoulli" . "jonas@bernoul.li")
  :keywords
  '("convenience")
  :url "https://github.com/emacscollective/no-littering")
;; Local Variables:
;; no-byte-compile: t
;; End:

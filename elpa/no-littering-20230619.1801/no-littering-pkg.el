(define-package "no-littering" "20230619.1801" "Help keeping ~/.config/emacs clean"
  '((emacs "25.1")
    (compat "29.1.4.1"))
  :commit "4eef1a140f22ec700e7045b22352649bfaef72f3" :authors
  '(("Jonas Bernoulli" . "jonas@bernoul.li"))
  :maintainers
  '(("Jonas Bernoulli" . "jonas@bernoul.li"))
  :maintainer
  '("Jonas Bernoulli" . "jonas@bernoul.li")
  :keywords
  '("convenience")
  :url "https://github.com/emacscollective/no-littering")
;; Local Variables:
;; no-byte-compile: t
;; End:

(define-package "transient" "20231120.1809" "Transient commands"
  '((emacs "26.1")
    (compat "29.1.4.1")
    (seq "2.24"))
  :commit "43e9c16d0c6e2edd0645a2f80bfab262ddda4e65" :authors
  '(("Jonas Bernoulli" . "jonas@bernoul.li"))
  :maintainers
  '(("Jonas Bernoulli" . "jonas@bernoul.li"))
  :maintainer
  '("Jonas Bernoulli" . "jonas@bernoul.li")
  :keywords
  '("extensions")
  :url "https://github.com/magit/transient")
;; Local Variables:
;; no-byte-compile: t
;; End:

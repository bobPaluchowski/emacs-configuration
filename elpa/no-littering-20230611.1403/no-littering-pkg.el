(define-package "no-littering" "20230611.1403" "Help keeping ~/.config/emacs clean"
  '((emacs "25.1")
    (compat "29.1.4.1"))
  :commit "b9c69118991070814081459238ee74857ec99ad9" :authors
  '(("Jonas Bernoulli" . "jonas@bernoul.li"))
  :maintainers
  '(("Jonas Bernoulli" . "jonas@bernoul.li"))
  :maintainer
  '("Jonas Bernoulli" . "jonas@bernoul.li")
  :keywords
  '("convenience")
  :url "https://github.com/emacscollective/no-littering")
;; Local Variables:
;; no-byte-compile: t
;; End:

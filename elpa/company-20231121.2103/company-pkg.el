(define-package "company" "20231121.2103" "Modular text completion framework"
  '((emacs "25.1"))
  :commit "dde921a83912a055b09c193d29f499910efb1cd8" :authors
  '(("Nikolaj Schumacher"))
  :maintainers
  '(("Dmitry Gutov" . "dmitry@gutov.dev"))
  :maintainer
  '("Dmitry Gutov" . "dmitry@gutov.dev")
  :keywords
  '("abbrev" "convenience" "matching")
  :url "http://company-mode.github.io/")
;; Local Variables:
;; no-byte-compile: t
;; End:

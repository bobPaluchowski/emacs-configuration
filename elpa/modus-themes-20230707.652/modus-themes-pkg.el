(define-package "modus-themes" "20230707.652" "Elegant, highly legible and customizable themes"
  '((emacs "27.1"))
  :commit "4e331d27dec9eb6d61a0fdf87aba1942966efbdf" :authors
  '(("Protesilaos Stavrou" . "info@protesilaos.com"))
  :maintainers
  '(("Modus-Themes Development" . "~protesilaos/modus-themes@lists.sr.ht"))
  :maintainer
  '("Modus-Themes Development" . "~protesilaos/modus-themes@lists.sr.ht")
  :keywords
  '("faces" "theme" "accessibility")
  :url "https://git.sr.ht/~protesilaos/modus-themes")
;; Local Variables:
;; no-byte-compile: t
;; End:

(define-package "doom-modeline" "20230811.236" "A minimal and modern mode-line"
  '((emacs "25.1")
    (compat "29.1.1.1")
    (nerd-icons "0.0.1")
    (shrink-path "0.2.0"))
  :commit "dc4fab35ca5d3c75b59caa61ba7fd78b576838ee" :authors
  '(("Vincent Zhang" . "seagle0128@gmail.com"))
  :maintainers
  '(("Vincent Zhang" . "seagle0128@gmail.com"))
  :maintainer
  '("Vincent Zhang" . "seagle0128@gmail.com")
  :keywords
  '("faces" "mode-line")
  :url "https://github.com/seagle0128/doom-modeline")
;; Local Variables:
;; no-byte-compile: t
;; End:

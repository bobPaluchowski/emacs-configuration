(define-package "nerd-icons" "20230920.543" "Emacs Nerd Font Icons Library"
  '((emacs "24.3"))
  :commit "48fa1b4484c38bf54d2716ca511881c878e36cf9" :authors
  '(("Hongyu Ding <rainstormstudio@yahoo.com>, Vincent Zhang" . "seagle0128@gmail.com"))
  :maintainers
  '(("Hongyu Ding <rainstormstudio@yahoo.com>, Vincent Zhang" . "seagle0128@gmail.com"))
  :maintainer
  '("Hongyu Ding <rainstormstudio@yahoo.com>, Vincent Zhang" . "seagle0128@gmail.com")
  :keywords
  '("lisp")
  :url "https://github.com/rainstormstudio/nerd-icons.el")
;; Local Variables:
;; no-byte-compile: t
;; End:

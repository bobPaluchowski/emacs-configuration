(define-package "all-the-icons-dired" "20230908.2208" "Shows icons for each file in dired mode"
  '((emacs "26.1")
    (all-the-icons "2.2.0"))
  :commit "28f6f6c478f230c27526ef7a91afdf82d472c24b" :authors
  '(("jtbm37"))
  :maintainers
  '(("Jimmy Yuen Ho Wong" . "wyuenho@gmail.com"))
  :maintainer
  '("Jimmy Yuen Ho Wong" . "wyuenho@gmail.com")
  :keywords
  '("files" "icons" "dired")
  :url "https://github.com/wyuenho/all-the-icons-dired")
;; Local Variables:
;; no-byte-compile: t
;; End:

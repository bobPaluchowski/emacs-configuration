(define-package "company" "20231006.1438" "Modular text completion framework"
  '((emacs "25.1"))
  :commit "22466f37e4dd3817f87b0aeec8b5d9a9ce538b98" :authors
  '(("Nikolaj Schumacher"))
  :maintainers
  '(("Dmitry Gutov" . "dmitry@gutov.dev"))
  :maintainer
  '("Dmitry Gutov" . "dmitry@gutov.dev")
  :keywords
  '("abbrev" "convenience" "matching")
  :url "http://company-mode.github.io/")
;; Local Variables:
;; no-byte-compile: t
;; End:

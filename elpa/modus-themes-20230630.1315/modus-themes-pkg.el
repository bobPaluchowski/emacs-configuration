(define-package "modus-themes" "20230630.1315" "Elegant, highly legible and customizable themes"
  '((emacs "27.1"))
  :commit "7ef62b72fcd17c4dbe8936922d27a19e8e3e7490" :authors
  '(("Protesilaos Stavrou" . "info@protesilaos.com"))
  :maintainers
  '(("Modus-Themes Development" . "~protesilaos/modus-themes@lists.sr.ht"))
  :maintainer
  '("Modus-Themes Development" . "~protesilaos/modus-themes@lists.sr.ht")
  :keywords
  '("faces" "theme" "accessibility")
  :url "https://git.sr.ht/~protesilaos/modus-themes")
;; Local Variables:
;; no-byte-compile: t
;; End:

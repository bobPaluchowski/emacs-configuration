(define-package "marginalia" "20231130.2350" "Enrich existing commands with completion annotations"
  '((emacs "27.1")
    (compat "29.1.4.0"))
  :commit "270d0d9bf766900e605883567b5131ae739918be" :authors
  '(("Omar Antolín Camarena <omar@matem.unam.mx>, Daniel Mendler" . "mail@daniel-mendler.de"))
  :maintainers
  '(("Omar Antolín Camarena <omar@matem.unam.mx>, Daniel Mendler" . "mail@daniel-mendler.de"))
  :maintainer
  '("Omar Antolín Camarena <omar@matem.unam.mx>, Daniel Mendler" . "mail@daniel-mendler.de")
  :keywords
  '("docs" "help" "matching" "completion")
  :url "https://github.com/minad/marginalia")
;; Local Variables:
;; no-byte-compile: t
;; End:

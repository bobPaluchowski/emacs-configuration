(define-package "vertico" "20230912.939" "VERTical Interactive COmpletion"
  '((emacs "27.1")
    (compat "29.1.4.0"))
  :commit "81bec39151f0a2fc6cad541f2290b4ab6282e6e2" :authors
  '(("Daniel Mendler" . "mail@daniel-mendler.de"))
  :maintainers
  '(("Daniel Mendler" . "mail@daniel-mendler.de"))
  :maintainer
  '("Daniel Mendler" . "mail@daniel-mendler.de")
  :keywords
  '("convenience" "files" "matching" "completion")
  :url "https://github.com/minad/vertico")
;; Local Variables:
;; no-byte-compile: t
;; End:

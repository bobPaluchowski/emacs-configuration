(define-package "doom-modeline" "20231024.1609" "A minimal and modern mode-line"
  '((emacs "25.1")
    (compat "29.1.4.2")
    (nerd-icons "0.1.0")
    (shrink-path "0.3.1"))
  :commit "2dc5f00b59565c7fa72b46d7d604a8c032e2d239" :authors
  '(("Vincent Zhang" . "seagle0128@gmail.com"))
  :maintainers
  '(("Vincent Zhang" . "seagle0128@gmail.com"))
  :maintainer
  '("Vincent Zhang" . "seagle0128@gmail.com")
  :keywords
  '("faces" "mode-line")
  :url "https://github.com/seagle0128/doom-modeline")
;; Local Variables:
;; no-byte-compile: t
;; End:

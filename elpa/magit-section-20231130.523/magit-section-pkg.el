(define-package "magit-section" "20231130.523" "Sections for read-only buffers."
  '((emacs "25.1")
    (compat "29.1.3.4")
    (dash "20221013"))
  :commit "53291c93e487c789cb7273088a72a76566fc8ed8" :authors
  '(("Jonas Bernoulli" . "jonas@bernoul.li"))
  :maintainer
  '("Jonas Bernoulli" . "jonas@bernoul.li")
  :keywords
  '("tools")
  :url "https://github.com/magit/magit")
;; Local Variables:
;; no-byte-compile: t
;; End:

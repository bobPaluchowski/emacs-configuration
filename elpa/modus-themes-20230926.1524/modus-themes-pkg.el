(define-package "modus-themes" "20230926.1524" "Elegant, highly legible and customizable themes"
  '((emacs "27.1"))
  :commit "284433625ed7cd3b19d2e34589c3041e16dd0deb" :authors
  '(("Protesilaos Stavrou" . "info@protesilaos.com"))
  :maintainers
  '(("Modus-Themes Development" . "~protesilaos/modus-themes@lists.sr.ht"))
  :maintainer
  '("Modus-Themes Development" . "~protesilaos/modus-themes@lists.sr.ht")
  :keywords
  '("faces" "theme" "accessibility")
  :url "https://git.sr.ht/~protesilaos/modus-themes")
;; Local Variables:
;; no-byte-compile: t
;; End:

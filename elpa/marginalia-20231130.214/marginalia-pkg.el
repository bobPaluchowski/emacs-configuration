(define-package "marginalia" "20231130.214" "Enrich existing commands with completion annotations"
  '((emacs "27.1")
    (compat "29.1.4.0"))
  :commit "8e2f682fd06dac24451c4349b18410fc8d6b4f9d" :authors
  '(("Omar Antolín Camarena <omar@matem.unam.mx>, Daniel Mendler" . "mail@daniel-mendler.de"))
  :maintainers
  '(("Omar Antolín Camarena <omar@matem.unam.mx>, Daniel Mendler" . "mail@daniel-mendler.de"))
  :maintainer
  '("Omar Antolín Camarena <omar@matem.unam.mx>, Daniel Mendler" . "mail@daniel-mendler.de")
  :keywords
  '("docs" "help" "matching" "completion")
  :url "https://github.com/minad/marginalia")
;; Local Variables:
;; no-byte-compile: t
;; End:

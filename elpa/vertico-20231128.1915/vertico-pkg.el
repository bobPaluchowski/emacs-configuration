(define-package "vertico" "20231128.1915" "VERTical Interactive COmpletion"
  '((emacs "27.1")
    (compat "29.1.4.4"))
  :commit "28cf80ed42539f6f649987797952b6db2c91d23f" :authors
  '(("Daniel Mendler" . "mail@daniel-mendler.de"))
  :maintainers
  '(("Daniel Mendler" . "mail@daniel-mendler.de"))
  :maintainer
  '("Daniel Mendler" . "mail@daniel-mendler.de")
  :keywords
  '("convenience" "files" "matching" "completion")
  :url "https://github.com/minad/vertico")
;; Local Variables:
;; no-byte-compile: t
;; End:

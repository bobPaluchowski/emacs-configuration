(define-package "vertico" "20230909.1944" "VERTical Interactive COmpletion"
  '((emacs "27.1")
    (compat "29.1.4.0"))
  :commit "436b8d02569bc8f7d684eb16733e038e9fa52467" :authors
  '(("Daniel Mendler" . "mail@daniel-mendler.de"))
  :maintainers
  '(("Daniel Mendler" . "mail@daniel-mendler.de"))
  :maintainer
  '("Daniel Mendler" . "mail@daniel-mendler.de")
  :keywords
  '("convenience" "files" "matching" "completion")
  :url "https://github.com/minad/vertico")
;; Local Variables:
;; no-byte-compile: t
;; End:

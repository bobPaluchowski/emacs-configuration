(define-package "doom-modeline" "20230725.1642" "A minimal and modern mode-line"
  '((emacs "25.1")
    (compat "28.1.1.1")
    (nerd-icons "0.0.1")
    (shrink-path "0.2.0"))
  :commit "b55d6cb731a9557e3eb30af9ea259b214b53622b" :authors
  '(("Vincent Zhang" . "seagle0128@gmail.com"))
  :maintainers
  '(("Vincent Zhang" . "seagle0128@gmail.com"))
  :maintainer
  '("Vincent Zhang" . "seagle0128@gmail.com")
  :keywords
  '("faces" "mode-line")
  :url "https://github.com/seagle0128/doom-modeline")
;; Local Variables:
;; no-byte-compile: t
;; End:

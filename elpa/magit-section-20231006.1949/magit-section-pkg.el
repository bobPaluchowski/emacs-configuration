(define-package "magit-section" "20231006.1949" "Sections for read-only buffers."
  '((emacs "25.1")
    (compat "29.1.3.4")
    (dash "20221013"))
  :commit "a0ef7bef31df93d665500fb52a18eecf3920a36d" :authors
  '(("Jonas Bernoulli" . "jonas@bernoul.li"))
  :maintainer
  '("Jonas Bernoulli" . "jonas@bernoul.li")
  :keywords
  '("tools")
  :url "https://github.com/magit/magit")
;; Local Variables:
;; no-byte-compile: t
;; End:

;;----------------------------------------------------
;; overriding image.el function image-type-available-p
;;----------------------------------------------------
(defun image-type-available-p (type)
  "Return t if image type TYPE is available.
  Images types are symbols like `xbm' or `jpeg'."
  (if (eq 'svg type)
    nil
    (and (fboundp 'init-image-library)
         (init-image-library type))))

;; Minimal UI
(menu-bar-mode t)	       
(tool-bar-mode t)
(scroll-bar-mode -1)
(setq inhibit-startup-message t
      initial-scratch-message nil)
(setq make-backup-files nil)
;;(modus-themes-load-operandi)

;;-----------------------------------------------------
;; stop creating backup files
;;-----------------------------------------------------
(setq make-backup-files nil)

;;-----------------------------------------------------
;; REMAPPING '#'
;;-----------------------------------------------------
(defun insert-hash-symbol ()
  "Insert a # symbol."
  (interactive)
  (insert "#"))

(global-set-key (kbd "C-c h") 'insert-hash-symbol)

;; auto-fill-mode AND visual-line-mode
;; Enable auto-fill-mode in all major modes
(defun my-auto-fill-mode ()
  (setq fill-column 100)
  (auto-fill-mode 1))

(add-hook 'text-mode-hook 'my-auto-fill-mode)


;;----------------------------------------------------
;; remapping M+g g to C+M+g g for 'go to line command'
;;----------------------------------------------------
(global-set-key (kbd "C-M-g g") 'goto-line)
(global-unset-key (kbd "M-g g"))

;;----------------------------------------------------
;; maximize emacs on startup
;;----------------------------------------------------
(add-to-list 'initial-frame-alist '(fullscreen . t))
(setq inhibit-startup-message t)
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-safe-themes
   '("17bd04719213ed7482ce37d8207f3618f55a81babe56484851ea5951ced383ef" display-line-numbers-type 'relative)))

;;-----------------------------------------------------
;; gc - the default is 800 kilobytes, measured in bytes
;;-----------------------------------------------------
(setq gc-cons-threshhold (* 50 1000 1000))

;;-----------------------------------------------------
;; initialize package sources
;;-----------------------------------------------------
(require 'package)

(setq package-archives '(("melpa" . "https://melpa.org/packages/")
			 ("org" . "https://orgmode.org/elpa/")
			 ("elpa" . "https://elpa.gnu.org/packages/")))

(package-initialize)
(unless (package-installed-p 'use-package)
  (package-install 'use-package))

(require 'use-package)
(setq use-package-always-ensure t)

(use-package auto-package-update
	     :custom
	     (auto-package-update-interval 7)
	     (auto-package-update-prompt-before-update t)
	     (auto-package-update-hide-results t)
	     :config
	     (auto-package-update-maybe)
	     (auto-package-update-at-time "09:00"))

(use-package no-littering)

(setq auto-save-file-name-fransforms
      `((".*" ,(no-littering-expand-var-file-name "auto-save/") t)))


;;-----------------------------------------------------
;; xah-fly-keys
;;----------------------------------------------------
(load-file "~/.emacs.d/elpa/xah-fly-keys-24.13.20231025112537/xah-fly-keys.el")
;;-----------------------------------------------------
;; vertico
;;-----------------------------------------------------
;;(use-package vertico
;;  :init
;;    (vertico-mode))
  ;;:ensure t
  ;;:config
(require 'vertico)
(vertico-mode)

;;-----------------------------------------------------
;; vertico-posframe
;;-----------------------------------------------------
(require 'vertico-posframe)
(vertico-posframe-mode 1)
;;(use-package vertico-posframe
;;:after vertico
;;:config
;;(vertico-posframe-mode 1)
;;(setq vertico-posframe-parameters
;;      '((left-fringe . 8)
;;        (right-fringe . 8))))

;;-----------------------------------------------------
;; Marginalia
;;-----------------------------------------------------
(use-package marginalia
  :bind (:map minibuffer-local-map
	      ("M-A" . marginalia-cycle))
  :init
  (marginalia-mode))

;;-----------------------------------------------------
;; NeoTree
;;-----------------------------------------------------
(add-to-list 'load-path "~/emacs/packages/neotree")
(require 'neotree)
(global-set-key [f8] 'neotree-toggle)

;; neo-smart-open
(setq neo-smart-open t)

;;-----------------------------------------------------
;; Give Some breathing room
;;-----------------------------------------------------
(set-fringe-mode 10)

;;-----------------------------------------------------
;; set up the visible bell
;;-----------------------------------------------------
(setq visible-bell t)

;;-----------------------------------------------------
;; doom modeline
;;-----------------------------------------------------
(use-package doom-modeline
	     :init (doom-modeline-mode 1)
	     :custom ((doom-modeline-height 25)))

;;-----------------------------------------------------
;; TAB bar mode
;;-----------------------------------------------------
;;(tab-bar-mode 1)
(desktop-save-mode 1)
(setq tab-bar-show 1)                    ;; hide bar if <= 1 tabs open
(setq tab-bar-close-button-show t) ;; if nil, no close button icon
(setq tab-bar-new-button-show nil) ;; if t, show new tab button
(setq tab-bar-new-tab-choice "*scratch*") ;; creates new scratch tab
(setq tab-bar-close-last-tab-choice 'tab-bar-mode-disable)
(setq tab-bar-close-tab-select 'recent)
(setq tab-bar-new-tab-to 'right)
(setq tab-bar-tab-hints nil)
(setq tab-bar-separator "")

;; customize the tab bar format to add the global mode line string
(setq tab-bar-format '(tab-bar-format-tabs tab-bar-separator
					   tab-bar-format-align-right
					   tab-bar-format-global))

;; menubar in the tab
;; (add-to-list 'tab-bar-format #'tab-bar-format-menu-bar)

;; custom tab colors
(set-face-attribute 'tab-bar-tab nil
		    :background "dark orange" :foreground "black")
(set-face-attribute 'tab-bar-tab-inactive nil
		    :background "gray" :foreground "black")


;; tab bar menu bar button
(setq tab-bar-menu-bar-button "👾")
;; (setq tab-bar-menu-bar-button "*")


;;-----------------------------------------------------
;; hl-line
;;-----------------------------------------------------
(global-hl-line-mode 1)
(set-face-background 'hl-line "#ECECEC")

;;-----------------------------------------------------
;; which-key
;;-----------------------------------------------------
(use-package which-key
	     :defer 0
	     :diminish which-hey-mode
	     :config
	     (which-key-mode)
	     (setq which-key-idle-delay 1))

;;-----------------------------------------------------
;; ivy - search completion in minibuffer
;;-----------------------------------------------------
(use-package ivy
	     :diminish
	     :bind (("C-s" . swiper)
		    :map ivy-minibuffer-map
		    ("TAB" . ivy-alt-done)
		    ("C-l" . ivy-alt-done)
		    ("C-j" . ivy-next-line)
		    ("C-k" . ivy-previous-line)
		    :map ivy-switch-buffer-map
		    ("C-k" . ivy-previous-line)
		    ("C-l" . ivy-done)
		    ("C-d" . ivy-switch-buffer-kill)
		    :map ivy-reverse-i-search-map
		    ("C-k" . ivy-previous-line)
		    ("C-d" . ivy-reverse-i-search-kill))
	     :config
	     (ivy-mode 1))

(use-package ivy-rich
	     :after ivy
	     :init
	     (ivy-rich-mode 1))

;;-----------------------------------------------------
;; counsel - ivy enhanced
;;-----------------------------------------------------
(use-package counsel
	     :bind (("C-M-j" . 'counsel-switch-buffer)
		    :map minibuffer-local-map
		    ("C-r" . 'counsel-minibuffer-history))
;;	     :custom
	     ;;	     (counsel-linux-app-format-function #'counsel-linux-app-format-function-name-only)
	     :config
	     (counsel-mode 1))

;;-----------------------------------------------------
;;ivy-prescient
;;-----------------------------------------------------
(use-package ivy-prescient
	     :after counsel
	     :custom
	     (ivy-prescient-enable-filtering nil)
	     :config
	     ;; uncomment the following line to have sorting enabled across sessions
	     ;;(prescient-persist-mode 1)
	     (ivy-prescient-mode 1))

;;-----------------------------------------------------
;; helpful
;;-----------------------------------------------------
(use-package helpful
	     :commands (helpful-callable helpful-variable helpful-command helpful-key)
	     :custom
	     (counsel-describe-function-function #'helpful-callable)
	     (counsel-describe-variable-function #'helpful-variable)
	     :bind
	     ([remap describe-function] . counsel-describe-function)
	     ([remap describe-command] . helpful-command)
	     ([remap describe-variable] . counsel-describe-variable)
	     ([remap describe-key] . counsel-key))


;;-----------------------------------------------------
;; org
;;-----------------------------------------------------
(defun efs/org-mode-setup ()
;;  (org-indent-mode)
  (variable-pitch-mode 1))

(use-package org
	     :pin org
	     :commands (org-capture org-agenda)
	     :hook (org-mode . efs/org-mode-setup)
	     :config
	     (setq org-ellipsis " ▾")

	     (setq org-agenda-start-with-log-mode t)
	     (setq org-log-done 'time)
	     (setq org-log-into-drawer t)

	     (setq org-agenda-files
		   '("~/org/*.org"))
	     (setq org-todo-keywords '((type "TODO" "NEXT" "WAITING" "DONE"))))

;; files with an extension .org to open in org-mode
(add-to-list 'auto-mode-alist '("\\.org$" . org-mode))
;;(add-to-list 'auto-mode-alist '("\\.org\\'" . org-mode)) ;; a variation of the previous line

;; filies within /org directory with an .org extantion will be
;; recognized by org-agenda
(setq org-agenda-files (directory-files-recursively "~/org/"
 "\\`[^.].*\\.org\\'"))
;;(setq org-agenda-files (directory-files-recursively "~/org/" "\\.org\\'"))


;;-----------------------------------------------------
;; org-bullets
;;-----------------------------------------------------
(use-package org-bullets
  :hook (org-mode . org-bullets-mode)
  :custom
  (org-bullets-bullet-list '("◉" "○" "●" "○" "●" "○" "●")))

(defun efs/org-mode-visual-fill ()
  (setq visual-fill-column-width 100
        visual-fill-column-center-text t)
  (visual-fill-column-mode 1))

;;--------------------------------------------------
;; org-roam
;;-----------------------------------------------------
(use-package org-roam
  :ensure t
  :init
  (setq org-roam-v2-ack t)
  :custom
  (org-roam-directory "~/RoamNotes")
  (org-roam-completion-everywhere t)
  (org-roam-capture-templates
   '(("p" "project" plain "* Goals\n\n%?\n\n* Tasks\n\n** TODO Add
initial tasks\n\n* Dates\n\n"
      :if-new (file+head "%<%Y%m%d%H%M%S>-${slug}.org" "#+title:
${title}\n#+filetags: Project")
      :unnarrowed t)))
  :bind (("C-c n l" . org-roam-buffer-toggle)
	 ("C-c n f" . org-roam-node-find)
	 ("C-c n i" . org-roam-node-insert)
	 :map org-mode-map
	 ("C-M-i" . completion-at-point))
  :config
  (org-roam-setup))

;; denote
(require 'denote)

;; Remember to check the doc strings of those variables.
(setq denote-directory (expand-file-name "~/Documents/notes/"))
(setq denote-known-keywords '("programming" "philosophy" "meditation" "politics" "drawing" "general"))
(setq denote-infer-keywords t)
(setq denote-sort-keywords t)
(setq denote-file-type nil) ; Org is default, set others here
(setq denote-prompts '(title keywords))
(setq denote-excluded-directories-regexp nil)
(setq denote-excluded-keywords-regexp nil)

;; Pick dates, where relevant, with Org's advanced interface:
(setq denote-date-prompt-use-org-read-date t)

;; Read this manual for how to specify 'denote-templates'. We do not include
;; an example here to avoid potential confusion.

(setq denote-date-format nil) ; read doc string

;; By default, we do not show the context of links. We just display
;; file names. This provides a more informative view.
(setq denote-backlinks-show-context t)

;; Also see 'denote-link-backlinks-display-buffer-action' which is a bit
;; advanced.

;; If you use Markdown of plain text files (Org renders links as buttons
;; right away)
(add-hook 'find-file-hook #'denote-link-buttonize-buffer)

;; We use different ways to specify a path for demo purposes.
(setq denote-dired-directories
      (list denote-directory
	    (thread-last denote-directory (expand-file-name "attachments"))
	    (expand-file-name "~/Documents/books")))

;; Generic (great if you rename files Denote-style in lots of places):
;; (add-hook 'dired-mode-hook #'denote-dired-mode)
;;
;; OR if only want it in 'denotee-dired-directories':
(add-hook 'dired-mode-hook #'denote-dired-mode-in-directories)

;; Automatically rename Denote buffers using the 'denote-rename-buffer-
;; format'.
(denote-rename-buffer-mode 1)

;; Denote DOES NOT define any key bindings. This is for the user to decide.
;; For example:
(let ((map global-map))
  (define-key map (kbd "C-c n n") #'denote)
  (define-key map (kbd "C-c n c") #'denote-region) ; "contents" mnemonic
  (define-key map (kbd "C-c n N") #'denote-type)
  (define-key map (kbd "C-c n d") #'denote-date)
  (define-key map (kbd "C-c n z") #'denote-signature) ; zettelkastel" mnemonic
  (define-key map (kbd "C-c n s") #'denote-subdirectory)
  (define-key map (kbd "C-c n t") #'denote-template)
  ;; If you intend to use Denote with a variety of files, it is
  ;; easier to bind the linkk-related commands to the 'global-map', as
  ;; shown here. Otherwise follow the same pattern for 'org-mode-map',
  ;; 'markdown-mode-map', and/or 'text-mode-map'.
  (define-key map (kbd "C-c n i") #'denote-link) ; "insert" mnemonic
  (define-key map (kbd "C-c n I") #'denote-add-links)
  (define-key map (kbd "C-c n b") #'denote-backlinks)
 ;; (define-key map (kbd "C-c n f f") #'denote-find-link)
  ;;(define-key map (kbd "C-c n f b") #'denote-find-backlink)
  ;; Note that 'denote-rename-file' can work form any context, not just
  ;; Dire buffers. That is why we bind it here to the 'global-map'.
  (define-key map (kbd "C-c n r") #'denote-rename-file)
  (define-key map (kbd "C-c n R") #'denote-rename-file-using-front-matter))
  ;; Key binbings specifically for Dired.
(let ((map dired-mode-map))
  (define-key map (kbd "C-c C-d C-i") #'denote-link-dired-marked-notes)
  (define-key map (kbd "C-c C-d C-r") #'denote-link-dired-rename-files)
  (define-key map (kbd "C-c C-d C-k") #'denote-dired-rename-marked-files-with-keywords)
  (define-key map (kbd "C-c C-d C-R") #'denote-dired-rename-marked-files-using-front-matter))

(with-eval-after-load 'org-capture
  (setq denote-org-capture-specifiers "%l\n%i\n%?")
  (add-to-list 'org-capture-templates
	       '("n" "New note (with denote.el)" plain
		 (file denote-last-path)
		 #'denote-org-capture
		 :no-save t
		 :immediate-finish nil
		 :kill-buffer t
		 :jump-to-captured t)))

;; Also check the commands 'denote-link-after-creating',
;; 'denote-link-or-create'. You may want to bind them to keys as well.

;; If you want to have Denote commands available via a right click
;; context menu, use the following and then enable
;; 'context-menu-mode'.
(add-hook 'context-menu-functions #'denote-context-menu)
		 
  
;;-----------------------------------------------------
;; org-modern
;;-----------------------------------------------------

;;-----------------------------------------------------
;; company - text completion framework
;;-----------------------------------------------------
;;(add-hook 'after-init-hook 'global-company-mode)

;;(use-package company-box
;;	     :hook (company-mode . company-box-mode))

;;-----------------------------------------------------
;; projectile - project management tool
;;-----------------------------------------------------
(use-package projectile
  :diminish projectile-mode
  :config (projectile-mode)
  :custom ((projectile-completion-system 'ivy))
  :bind-keymap
  ("C-c p" . projectile-command-map)
  :init
  ;; NOTE: Set this to the folder where you keep your Git repos!
  (when (file-directory-p "~/Projects/Code")
    (setq projectile-project-search-path '("~/Projects/Code")))
  (setq projectile-switch-project-action #'projectile-dired))

(use-package counsel-projectile
  :after projectile
  :config (counsel-projectile-mode))

;;-----------------------------------------------------
;; magit
;;-----------------------------------------------------
(use-package magit
	     :commands magit-status
	     :custom
	     (magit-display-buffer-function #'magit-display-buffer-same-window-except-diff-v1))

;;-----------------------------------------------------
;; rainbow-delimiters
;;-----------------------------------------------------
(use-package rainbow-delimiters
	     :hook (prog-mode . rainbow-delimiters-mode))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(default ((t (:family "Menlo" :foundry "nil" :slant normal :weight regular :height 140 :width normal)))))
